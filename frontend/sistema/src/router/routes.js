
const routes = [
  {
    path: '/',
    name: 'CadastroLoja',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/CadastroLoja.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
