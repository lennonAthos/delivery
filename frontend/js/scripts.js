var app = new Vue({
  el: '#app',
  data: {
    key: 'AIzaSyB9_eDBJoM08jEt5opRfHFJaWfzN2DCiJ8',
    cep: '',
    lojas: []
  },
  mounted () {
    MicroModal.init()
  },
  methods: {
    buscaCep () {
      axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${this.cep}&key=${this.key}`)
        .then(response => {
          const {lat, lng} = response.data.results[0].geometry.location
          this.lojasProximas(lat, lng)
        })
        .catch(error => {
          console.log(error)
        })
    },
    lojasProximas (lat, long) {
      axios.get(`http://127.0.0.1:3333/lojasproximas/${lat}/${long}/10`)
        .then(response => {
          this.lojas = response.data
          MicroModal.show('modal-1')
          console.log(response.data)
        })
        .catch(error => {
          consolelog(error)
        })
    }
  }
})
