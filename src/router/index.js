import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

function load (component) {
  return () => import(`@/pages/${component}.vue`)
}

export default new Router({
  routes: [
    {path: '/', redirect: {name: 'logistaHome'}},
    {
      path: '/logista',
      name: 'TemplateLogista',
      component: load('TemplateLogista'),
      children: [
        {
          name: 'logistaHome',
          path: 'home',
          component: load('logista/Home')
        }
      ]
    }
  ]
})
