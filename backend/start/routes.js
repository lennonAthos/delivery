'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

Route.resource('lojas', 'LojaController').apiOnly()

Route.resource('clientes', 'ClienteController').apiOnly()

Route.resource('pedidos', 'PedidoController').apiOnly()

Route.resource('itens', 'ItenController').apiOnly()

Route.resource('itensHasPedido', 'ItensHasPedidoController').apiOnly()

Route.get('lojasproximas/:lat/:long/:raio', 'LojaController.lojasProximas')
