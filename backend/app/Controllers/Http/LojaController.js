'use strict'

const Loja = use('App/Models/Loja')
const Database = use('Database')

/**
 * Resourceful controller for interacting with lojas
 */
class LojaController {
  /**
   * Show a list of all lojas.
   * GET lojas
   */
  async index ({ request, response, view }) {
    const data = await Loja.all()

    return data
  }

  /**
   * Create/save a new loja.
   * POST lojas
   */
  async store ({ request, response }) {
    const values = request.all()

    const data = await Loja.create(values)

    return data
  }

  /**
   * Display a single loja.
   * GET lojas/:id
   */
  async show ({ params, request, response, view }) {
    const data = Loja.findOrFail(params.id)

    return data
  }

  /**
   * Update loja details.
   * PUT or PATCH lojas/:id
   */
  async update ({ params, request, response }) {
    const loja = await Loja.findOrFail(params.id)

    const values = request.all()

    loja.merge(values)

    await loja.save()

    return loja
  }

  /**
   * Delete a loja with id.
   * DELETE lojas/:id
   */
  async destroy ({ params, request, response }) {
    const loja = await Loja.findOrFail(params.id)

    const data = await loja.delete()

    return data
  }

  async lojasProximas ({params, request}) {

    const lat = params.lat
    const long = params.long
    const raio = params.raio

    const data = await Database.raw(`
      SELECT nome,
            ( 6371 * acos( cos( radians(${lat}) ) * cos( radians( latitude ) )
            * cos( radians(longitude) - radians(${long})) + sin(radians(${lat}))
            * sin( radians(latitude)))) AS distance
      FROM lojas
      HAVING distance < ${raio}
      ORDER BY distance`)

      return data[0]
  }
}

module.exports = LojaController
