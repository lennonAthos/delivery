'use strict'

const Cliente = use('App/Models/Cliente')

/**
 * Resourceful controller for interacting with clientes
 */
class ClienteController {
  /**
   * Show a list of all clientes.
   * GET clientes
   */
  async index ({ request, response, view }) {
    const data = Cliente.all()

    return data
  }

  /**
   * Create/save a new cliente.
   * POST clientes
   */
  async store ({ request, response }) {
    const values = request.all()

    const data = await Cliente.create(values)

    return data
  }

  /**
   * Display a single cliente.
   * GET clientes/:id
   */
  async show ({ params, request, response, view }) {
    const data = await Cliente.findOrFail(params.id)

    return data
  }

  /**
   * Update cliente details.
   * PUT or PATCH clientes/:id
   */
  async update ({ params, request, response }) {
    const cliente = await Cliente.findOrFail(params.id)

    const values = request.all()

    cliente.merge(values)

    await cliente.save()

    return cliente
  }

  /**
   * Delete a cliente with id.
   * DELETE clientes/:id
   */
  async destroy ({ params, request, response }) {
    const cliente = await Cliente.findOrFail(params.id)

    const data = await cliente.delete()

    return data
  }
}

module.exports = ClienteController
