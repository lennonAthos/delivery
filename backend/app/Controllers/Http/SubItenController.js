'use strict'

/**
 * Resourceful controller for interacting with subitens
 */
class SubItenController {
  /**
   * Show a list of all subitens.
   * GET subitens
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new subiten.
   * GET subitens/create
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new subiten.
   * POST subitens
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single subiten.
   * GET subitens/:id
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing subiten.
   * GET subitens/:id/edit
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update subiten details.
   * PUT or PATCH subitens/:id
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a subiten with id.
   * DELETE subitens/:id
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = SubItenController
