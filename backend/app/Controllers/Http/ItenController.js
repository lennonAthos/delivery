'use strict'

const Iten = use('App/Models/Iten')

/**
 * Resourceful controller for interacting with itens
 */
class ItenController {
  /**
   * Show a list of all itens.
   * GET itens
   */
  async index ({ request, response, view }) {
    const data = await Iten.all()

    return data
  }

  /**
   * Create/save a new iten.
   * POST itens
   */
  async store ({ request, response }) {
    const values = request.all()

    const data = await Iten.create(values)

    return data
  }

  /**
   * Display a single iten.
   * GET itens/:id
   */
  async show ({ params, request, response, view }) {
    const data = await Iten.findOrFail(params.id)

    return data
  }

  /**
   * Update iten details.
   * PUT or PATCH itens/:id
   */
  async update ({ params, request, response }) {
    const cliente = await Iten.findOrFail(params.id)

    const values = request.all()

    cliente.merge(values)

    await cliente.save()

    return cliente
  }

  /**
   * Delete a iten with id.
   * DELETE itens/:id
   */
  async destroy ({ params, request, response }) {
    const item = await Iten.findOrFail(params.id)

    const data = await item.delete()

    return data
  }
}

module.exports = ItenController
