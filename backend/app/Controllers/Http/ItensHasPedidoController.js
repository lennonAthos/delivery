'use strict'

const ItensHasPedido = use('App/Models/ItensHasPedido')

/**
 * Resourceful controller for interacting with itenshaspedidos
 */
class ItensHasPedidoController {
  /**
   * Show a list of all itenshaspedidos.
   * GET itenshaspedidos
   */
  async index ({ request, response, view }) {
    const data = await ItensHasPedido.all()

    return data
  }

  /**
   * Create/save a new itenshaspedido.
   * POST itenshaspedidos
   */
  async store ({ request, response }) {
    const itens = request.collect(['id_item', 'id_pedido'])

    const data = await ItensHasPedido.createMany(itens)

    return data
  }

  /**
   * Display a single itenshaspedido.
   * GET itenshaspedidos/:id
   */
  async show ({ params, request, response, view }) {
    const data = await ItensHasPedido.findOrFail(params.id)

    return data
  }

  /**
   * Update itenshaspedido details.
   * PUT or PATCH itenshaspedidos/:id
   */
  async update ({ params, request, response }) {
    const cliente = await ItensHasPedido.findOrFail(params.id)

    const values = request.all()

    cliente.merge(values)

    await cliente.save()

    return cliente
  }

  /**
   * Delete a itenshaspedido with id.
   * DELETE itenshaspedidos/:id
   */
  async destroy ({ params, request, response }) {
    const itemPedido = await ItensHasPedido.findOrFail(params.id)

    const data = await itemPedido.delete()

    return data
  }
}

module.exports = ItensHasPedidoController
