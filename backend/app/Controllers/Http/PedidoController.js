'use strict'

const Pedido = use('App/Models/Pedido')
const ItensHasPedido = use('App/Models/ItensHasPedido')

/**
 * Resourceful controller for interacting with pedidos
 */
class PedidoController {
  /**
   * Show a list of all pedidos.
   * GET pedidos
   */
  async index ({ request, response, view }) {
    const data = Pedido.all()

    return data
  }

  /**
   * Create/save a new pedido.
   * POST pedidos
   */
  async store ({ request, response }) {
    const pedidoValues = request.except(['itens'])
    const pedidoSave = await Pedido.create(pedidoValues)

    let PedidoItens = request.only(['itens'])

    PedidoItens['itens'].map(values => {
      values.id_pedido = pedidoSave.id

      return values
    })

    const itenSave = await ItensHasPedido.createMany(PedidoItens['itens'])

    return itenSave
  }

  /**
   * Display a single pedido.
   * GET pedidos/:id
   */
  async show ({ params, request, response, view }) {
    const data = await Pedido.findOrFail(params.id)

    return data
  }

  /**
   * Update pedido details.
   * PUT or PATCH pedidos/:id
   */
  async update ({ params, request, response }) {
    const cliente = await Pedido.findOrFail(params.id)

    const values = request.all()

    cliente.merge(values)

    await cliente.save()

    return cliente
  }

  /**
   * Delete a pedido with id.
   * DELETE pedidos/:id
   */
  async destroy ({ params, request, response }) {
    const pedido = await Pedido.findOrFail(params.id)

    const data = await pedido.delete()

    return data
  }
}

module.exports = PedidoController
